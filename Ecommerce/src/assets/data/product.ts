interface Product {
  id: number;
  name: string;
  price: number;
  description: string;
  image: string;
  mrp: number;
  rating: number;
  discount: number;
  quantity: number
}

const products = [
  {
    "id": 1,
    "name": "Apple Iphone 13",
    "mrp": 78399,
    "price": 69999,
    "specs": "6 GB RAM 64GB ROM",
    "description": "Cinematic mode now in 4K Dolby Vision up to 30 fps",
    "image": "/assets/pics/iphone.jpg",
    "rating": 4.6,
    "discount": 12,
    "quantity": 1
  },
  {
    "id": 2,
    "name": "Google Pixel 4a",
    "mrp": 43999,
    "price": 39999,
    "specs": "4GB RAM 64GB ROM",
    "description": "Pixel’s 12 megapixel camera, Motion Mode, and Portrait Mode",
    "image": "/assets/pics/pixel.jpg",
    "rating": 4.0,
    "discount": 10,
    "quantity": 1
  },
  {
    "id": 3,
    "name": "OnePlus 9 Pro",
    "mrp": 87499,
    "price": 69999,
    "specs": "8 GB RAM 256GB ROM",
    "description": "6.7 Inches Fluid AMOLED Display with 120Hz refresh rate",
    "image": "/assets/pics/oneplus.jpg",
    "rating": 4.8,
    "discount": 25,
    "quantity": 1
  },
  {
    "id": 4,
    "name": "Samsung M12",
    "mrp": 38399,
    "price": 31999,
    "specs": "4 GB RAM 64GB ROM",
    "description": "Powerful MediaTek Helio P35 Octa Core 2.3GHz with Android 12",
    "image": "/assets/pics/samsung.jpg",
    "rating": 3.8,
    "discount": 20,
    "quantity": 1
  },
  {
    "id": 5,
    "name": "Nothing Phone 1",
    "mrp": 39999,
    "price": 32999,
    "specs": "8 GB RAM 256GB ROM",
    "description": "Glyph Interface | Qualcomn Snapdragon 778G+ Processor",
    "image": "/assets/pics/nothingphone.webp",
    "rating": 3.8,
    "discount": 20,
    "quantity": 1
  }
]

// console.log(JSON.parse(JSON.stringify(products)));
export {Product, products}
