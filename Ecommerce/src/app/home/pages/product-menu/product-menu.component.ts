import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {HomeService} from "../../home.service";
import {products, Product} from "../../../../assets/data/product";
import {SessionService} from "../../../services/session.service";

@Component({
  selector: 'app-product-menu',
  templateUrl: './product-menu.component.html',
  styleUrls: ['./product-menu.component.css']
})
export class ProductMenuComponent {

  products = products;
  cart: Product[] | any = [];
  existingProduct = false;

  @ViewChild('head') head: any;

  constructor(private homeService: HomeService,
              private sessionService: SessionService) {

  }

  //move products to wishlist
  addToWishlist(product: Product) {
    this.homeService.addToWishlist(product);
    window.alert('Your product has been added to the your Wishlist!');
  }

  // move product into shopping cart
  addToCart(product: Product) {
    this.existingProduct = false;
    if (this.sessionService.getCart()) {
      this.cart = this.sessionService.getCart();
      this.cart.forEach((cart: any) => {
        if (cart.id == product.id) {
          this.existingProduct = true;
        }
      })
    }
    if (!this.existingProduct) {
      this.sessionService.addToCart(product);
      alert('Your product has been added to the cart!');
    } else {
      alert('This product already exists in the cart!');
    }
  }


  //function to return list of numbers from 0 to n-1
  numSequence(n: number): Array<number> {
    return Array(n);
  }
}

